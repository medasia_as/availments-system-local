<!DOCTYPE html>
<html lang="en">
    <head>
        <script>
            $(document).ready(function () {
                $(".selectAll").click(function () {
                    var checked_status = this.checked;
                    $(".selMulti").each(function () {
                        this.checked = checked_status;
                    });
                });
            });
        </script>
    </head>
    <body>
        <?php echo validation_errors(); ?>
        <?php echo form_open('records/accounts/verifyPassword'); ?>
        <?php
        $template = array(
            'table_open' => '<table border="0" cellpadding="4" cellspacing="0">',
            'table_close' => '</table>'
        );
        $this->table->set_template($template);
        $multi = $this->table->add_row(form_submit(array('name' => 'submit', 'value' => 'Delete', 'class' => 'btn btn-danger')), form_checkbox(array('name' => 'selectAll', 'id' => 'selectAll', 'class' => 'selectAll')), form_label('Select All'));
        echo $this->table->generate($multi);
        echo '<div class="table_scroll table_list">';
        $tmpl = array(
            'table_open' => '<table border="1" cellpadding="4" cellspacing="0" class="table table-bordered table-hover">',
            'heading_row_start' => '<tr>',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th>',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );

        $this->table->set_template($tmpl);
        $this->table->set_heading('', 'Company Name', /*'Address', 'Street', 'City', 'State', 'Postal Code', 'Code',*/ 'Insurance', 'Members Count', 'Broker Name', 'Notes','Status');
        $count = 1;
        foreach ($company as $value => $key) {
            if($key['status'] == 'DELETED')
            {
                $color = 'red';
            }
            else
            {
                $color = 'black';
            }
            // Build the custom actions links.
            // $actions = anchor(base_url()."records/company/delete/".$key['id']."/", "Delete");
            $members = anchor(base_url() . "records/compins/members/" . $key['comp_ins'][0]['id'], "Members(" . $key['comp_ins'][0]['membercount'] . ")", array('target' => '_blank'));
            $selMulti = form_checkbox(array('name' => 'selMulti[]', 'id' => 'selMulti', 'class' => 'selMulti', 'value' => $key['id']));
            // Adding a new table row.
            $this->table->add_row(
                        "<font color=".$color.">".$count++ . "." .$selMulti,
                        "<font color=".$color.">".anchor(base_url() . "records/accounts/view/company/" . $key['id'] . "/", $key['name']),
//                        $key['address'],
//                        $key['street'], /**$key['vendor_account'],**/
//                        $key['city'],
//                        $key['state'],
//                        $key['postal_code'],
//                        $key['code'],
                        "<font color=".$color.">".$key['comp_ins'][0]['insurance'],
                        "<font color=".$color.">".$members,
                        "<font color=".$color.">".$key['comp_ins'][0]['broker_name'],
                        "<font color=".$color.">".$key['comp_ins'][0]['notes'],
                        "<font color=".$color.">".$key['status']
                    );
        }
        echo $this->table->generate();
        echo form_hidden('location', 'records/company');
        echo '</div>';
        ?>
    </body>
</html>