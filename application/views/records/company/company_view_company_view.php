<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Records of <?php echo $name?></title>
		<link href="<?php echo base_url();?>bootstrap/css/bootstrap.css" rel="stylesheet">
	
		<script>
		$(document).ready(function() {
			$('.editable').editable('<?=base_url()?>utils/ajaxeditinplace', {
				indicator : 'Saving...',
				cancel    : 'Cancel',
				submit    : 'OK',
				tooltip   : 'Click to edit...',
				onblur    : 'cancel',
				submitdata : {table: 'company', key: <?=$id?>}
			});
			$('.editable_broker').editable('<?=base_url()?>utils/ajaxeditinplace', {
                    indicator : 'Saving...',
                    cancel    : 'Cancel',
                    submit    : 'OK',
                    tooltip   : 'Click to edit...',
                    onblur    : 'cancel',
                    width     : '100%',
                    submitdata : {table: 'brokers', key: <?=$id?>}
                });
		});
		</script>
	</head>
	<body class="main-bg">

		<div class="profile_container">
			<div class="profile_name">Records of <?php echo $name?></div>
			<?php
				$tmpl = array (
								'table_open'          => '<table cellpadding="4" cellspacing="0" class="profile_table">',
								'table_close'         => '</table>'
								);
				$this->table->set_template($tmpl);
				$back = anchor(base_url()."records/company/", "Back");
				$this->table->add_row('Company Name', '<div class="editable" id="name">'.$name.'</div>');
                                
//                                $this->table->add_row('Address', '<div class="editable" id="address">'.$address.'</div>');
//                                $this->table->add_row('Street', '<div class="editable" id="street">'.$street.'</div>');
//                                $this->table->add_row('City', '<div class="editable" id="city">'.$city.'</div>');
//                                $this->table->add_row('State', '<div class="editable" id="state">'.$state.'</div>');
//                                $this->table->add_row('Postal Code', '<div class="editable" id="postal_code">'.$postal_code.'</div>');
//                                
//				$this->table->add_row('Code', '<div class="editable" id="code">'.$code.'</div>');
				// $this->table->add_row(anchor(base_url()."records/company/delete/".$id."/", "Delete Company", array('onClick'=>"return confirm('Are you sure you want to delete this record?')",'class'=>'btn btn-danger')),'');

				echo $this->table->generate();

				if(isset($broker))
				{
					echo '<h2>Broker Details</h2>';
					$this->table->set_template($tmpl);
		            $back = anchor(base_url()."records/company/", "Back");
		            $this->table->add_row('Name', '<div class="editable_broker" id="name">'.$broker[0]['name'].'</div>');
		            $this->table->add_row('Address', '<div class="editable_broker" id="address">'.$broker[0]['address'].'</div>');
		            $this->table->add_row('Street', '<div class="editable_broker" id="street">'.$broker[0]['street'].'</div>');
		            $this->table->add_row('City', '<div class="editable_broker" id="city">'.$broker[0]['city'].'</div>');
		            $this->table->add_row('State', '<div class="editable_broker" id="state">'.$broker[0]['state'].'</div>');
		            $this->table->add_row('Postal Code', '<div class="editable_broker" id="postal_code">'.$broker[0]['postal_code'].'</div>');
		            $this->table->add_row('Contact Person', '<div class="editable_broker" id="contact_person">'.$broker[0]['contact_person'].'</div>');
		            $this->table->add_row('Contact Number', '<div class="editable_broker" id="contact_no">'.$broker[0]['contact_no'].'</div>');
		            // $this->table->add_row(anchor(base_url()."records/accounts/delete/".$id."/", "Delete Broker", array('onClick'=>"return confirm('Are you sure you want to delete this record?')",'class'=>'btn btn-danger')),'');

		            echo $this->table->generate();
				}
			?>
		</div>
	</body>
</html>