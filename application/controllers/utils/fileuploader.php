<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
echo memory_get_usage();

/** Error reporting */
// error_reporting(E_ALL);
// ini_set('display_errors', TRUE);
// ini_set('display_startup_errors', TRUE);
// ini_set('memory_limit','150M');


class Fileuploader extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$models = array(
					    'records_model' => '',
					    'fileuploader_model' => ''
						);
		$this->load->model($models,'',TRUE);
		$this->load->library('csvimport');
	}
	function upto($tableuri)
	{
		// THESE LINES OF CODES ARE USED FOR UPLOADING TEMPLATES
// 		if($tableuri == 'templates')
// 		{
// 			$config['upload_path'] ='/home/dev/web/operations051513/files/uploads/'.$tableuri;
// 			$config['allowed_types'] = 'xls';
// 			$config['file_name'] = $this->encrypt->sha1(md5($_FILES['file']['name'].now()));
// 			$this->load->library('upload', $config);

// 			$ref = $this->input->server('HTTP_REFERER', TRUE); //previous page

// 			if ( ! file_exists($config['upload_path']))
// 			{
// 				mkdir($config['upload_path'], 0777); //make directory if it doesn't exist
// 			}
// 			if(file_exists($config['upload_path'].$config['file_name'] ))
// 			{
// 				//file exists
// 				//file exists return false with flash data
// 				$this->session->set_flashdata('result', 'ERROR: File already exists. Please contact IT Dept.');
// 				redirect($ref, 'location');
// 			}
// 			else
// 			{ //file does not exist. PROCEED.
// 				if($this->upload->do_upload('file'))
// 				{
// 					//upload success
// 					echo $this->upload->display_errors(); //display error(s) IF ANY
// 					$file = $this->upload->data(); //get details of uploaded file
// 					$this->excel_reader->read($file['full_path']); //read file using Excel Reader (3rd party) library

// 					$user = $this->session->userdata('logged_in');
// 					$filedata = array(
// 								'uploaded_to'	=> $this->uri->segment(4),
// 								'uploader'		=> $user['username'],
// 								'date_uploaded'	=> mdate('%Y-%m-%d', now()),
// 								'hash'			=> $file['raw_name'],
// 								'filename'		=> $file['client_name'],
// 								'path'			=> $file['file_path'],
// 								'size'			=> $file['file_size'].' kB'
// 									);
// 					$resultFileData = $this->fileuploader_model->insert($filedata);
// 					if($resultFileData)
// 					{
// 						/** Error reporting */
// 						error_reporting(E_ALL);
// 						ini_set('display_errors', TRUE);
// 						ini_set('display_startup_errors', TRUE);

// 						//insert filedata success
// 						//return true with flash data
// 						var_dump($config['upload_path']);
// 						$this->session->set_flashdata('result', 'Succesfully uploaded file.');
// 						redirect($ref, 'location');
// 					} 
// 					else
// 					{
// 						/** Error reporting */
// 						error_reporting(E_ALL);
// 						ini_set('display_errors', TRUE);
// 						ini_set('display_startup_errors', TRUE);

// 						//upload failed
// 						//return false with flash data
// 						var_dump($config['upload_path']);
// 						$this->session->set_flashdata('result', 'ERROR: Uploading file.'.$this->upload->display_errors().'Please contact IT Dept.');
// 						//var_dump($file);
// 						redirect($ref, 'location');
// 					}
// 				}
// 			}
// 		}
// 	}
// }
		$config['upload_path'] = '/home/dev/web/operations051513/files/uploads/'.$tableuri;
		$config['allowed_types'] = '*';
		$config['max_size'] = '1000';

		$config['file_name'] = $this->encrypt->sha1(md5($_FILES['file']['name'].now()));

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		$ref = $this->input->server('HTTP_REFERER', TRUE); //previous page

		switch($tableuri) {
			case 'company_insurance':
				$table = $tableuri;
				break;
			case 'diagnosis':
				$table = $tableuri;
				break;
			case 'company_insurance_members':
				$table = 'patient';
				break;
			case 'hospclinic':
				$table = 'hospital_test';
				break;
			case 'dentistsdoctors':
				$table = 'dentistsanddoctors';
				break;
			case 'emerroom':
				$table = 'emergency_room';
				break;
			case 'hospaccnt':
				$table = 'hospital_account';
				break;
			case 'new_member':
				$table = 'patient';
				break;
			default:
				$table = $tableuri;
				break;
		}

		if ( ! file_exists($config['upload_path']))
		{
			 mkdir($config['upload_path'], 0777); //make directory if it doesn't exist
		}
		if(file_exists($config['upload_path'].$config['file_name'] ))
		{ //file exists
			//file exists return false with flash data
			$this->session->set_flashdata('result',' <b>ERROR: File already exists. Please contact IT Dept.</b>');
			redirect($ref, 'location');
		}
		else
		{ //file does not exist. PROCEED.

			$csv = array('text/x-comma-separated-values',
				'text/comma-separated-values', 'application/octet-stream',
				'application/vnd.ms-excel', 'application/x-csv',
				'text/x-csv', 'text/csv', 'application/csv', 'application/excel');
				
			foreach($csv as $key => $value)
			{
				if($_FILES['file']['type'] == $value)
				{
					$file_allowed = TRUE;
				}
			}

			if($file_allowed == TRUE)
			{
				$date_created = date('Y-m-d');

				if($this->upload->do_upload('file'))
				{ //upload success
					// echo 'UPLOAD SUCCESSFUL';
					echo $this->upload->display_errors(); //display error(s) IF ANY
					$file = $this->upload->data(); //get details of uploaded file
					$data = $this->csvimport->get_array($file['full_path']); //read file using Excel Reader (3rd party) library

					if($tableuri == 'company_insurance_members')
					{
						$patient = array();
						$patient_company_insurance = array();
						$counter = 1;
						foreach($data as $key => $value)
						{
							$special_id = uniqid()."-".time()."-".$counter++;
							$user = $this->session->userdata('logged_in');
							$data[$key]['special_id'] = $special_id;
							$data[$key]['user'] = $user['name'];
							$data[$key]['age'] = computeAge($data[$key]['dateofbirth']);
							$data[$key]['date_encoded'] = $date_created;
							$data[$key]['cardholder_type'] == 'principal'? $data[$key]['cardholder'] = $data[$key]['lastname'].', '.$data[$key]['firstname'].' / '.$data[$key]['firstname'] : $data[$key]['cardholder']=$data[$key]['cardholder'];
							
							$patient[] = $data[$key];
							$patient_company_insurance[] = array(
												'patient_special_id' => $special_id,
												'company_insurance_id' => $_POST['company_insurance_id']
											);
						}

						$insert_id = $this->records_model->registerBatch('patient', $patient);
						$result = $this->records_model->registerBatch('patient_company_insurance', $patient_company_insurance);
					}
					else if($tableuri == 'company')
					{
						//insert each row ONE BY ONE
						//and get insert_id
						//to be used for patient_company_insurance
						$company = array();
						$company_insurance = array();
						$counter = 1;
						foreach($data as $key => $value)
						{
							//fix cardholder shit
							$special_id = uniqid()."-".time()."-".$counter++;
							$user = $this->session->userdata('logged_in');
							$data[$key]['special_id'] = $special_id;
							$data[$key]['encoded_by'] = $user['name'];
							$data[$key]['date_created'] = $date_created;

							$company_insurance[] = array('company' => $data[$key]['name'],
													'company_special_id' => $special_id,
													'insurance_id' => $_POST['insurance_id'],
													'insurance' => $_POST['compins-ins_upload'],
													'broker_id' => $_POST['broker_id'],
													'broker_name' => $_POST['broker_name'],
													'notes' => $data[$key]['notes'],
													'date_created' => $date_created,
													'encoded_by' => $user['name']
													);

							unset($data[$key]['start'], $data[$key]['end'], $data[$key]['notes']);
							$company[] = $data[$key];
						}
						$insert_id = $this->records_model->registerBatch($table, $company);
						$result = $this->records_model->registerBatch('company_insurance', $company_insurance);
					}
					else if($tableuri == 'dentistsdoctors')
					{
							//same as members BUT
						//use clinics as second table after getting insert id 
						foreach($data as $key => $value)
						{
							//MULTI UPLOAD NO CLINICS
							$user = $this->session->userdata('logged_in');
							$data[$key]['user'] = $user['name'];
							$result = $this->records_model->register('dentistsanddoctors', $data[$key]);
						}
					}
						// else if($tableuri == 'hospclinic')
						// {
						// 	foreach($data as $key => $value)
						// 	{
						// 		//MULTI UPLOAD NO CLINICS
						// 		$user = $this->session->userdata('logged_in');
						// 		$data[$key]['user'] = $user['name'];
						// 		$result = $this->records_model->register('hospital', $data[$key]);
						// 	}
						// }
						// else
						// {
						// 	$result = $this->records_model->registerBatch($table, $data);
						// }
					else
					{
						foreach($data as $key => $value)
						{
							$user = $this->session->userdata('logged_in');
							$data[$key]['encoded_by'] = $user['name'];
							$data[$key]['date_created'] = $date_created;

							$result = $this->records_model->register($table, $data[$key]);
						}
					}
					// var_dump($result);
					if($result)
					{ //insert batch success
						$user = $this->session->userdata('logged_in');
						$filedata = array(
									'uploaded_to'	=> $this->uri->segment(4),
									'uploader'		=> $user['username'],
									'date_uploaded'	=> mdate('%Y-%m-%d', now()),
									'hash'			=> $file['raw_name'],
									'filename'		=> $file['client_name'],
									'path'			=> $file['file_path'],
									'size'			=> $file['file_size'].' kB'
										);
						$resultFileData = $this->fileuploader_model->insert($filedata);
						if($resultFileData)
						{ //insert filedata success
							//return true with flash data
							$this->session->set_flashdata('result', '<b>Succesfully uploaded file.</b>');
							redirect($ref, 'location');
						} 
						else
						{
							//return false with flash data
							$this->session->set_flashdata('result', '<b>ERROR: Inserting filedata. Please contact IT Dept.</b>');
							redirect($ref, 'location');
						}
					}
					else
					{ 	//insert batch failed
						//return false with flash data
						$this->session->set_flashdata('result', '<b>ERROR: Inserting batch. Database Error. '.$this->upload->display_errors().' Please contact IT Dept.</b>');
						redirect($ref, 'location');
					} 
				}
				else
				{ 	//insert batch failed
					//return false with flash data
					$this->session->set_flashdata('result', '<b>ERROR: Inserting batch. Database Error. '.$this->upload->display_errors().' Please contact IT Dept.</b>');
					redirect($ref, 'location');
				} 
			}
			else
			{ //upload failed
				$this->session->set_flashdata('result', '<b>ERROR: Uploading file.'.$this->upload->display_errors().'Please contact IT Dept.</b>');
				//var_dump($file);
				redirect($ref, 'location');
			}
		}
	}

	function upload_pdf($table)
	{
		$config['upload_path'] = '/home/dev/web/operations051513/files/uploads/pdf/'.$table;
		$config['allowed_types'] = '*'; // SET TO ALL, SINCE THERE'S AN ERROR IN UPLOADING PDF FILE TYPE, SPECIAL VALIDATION ARE IN THE INNER PROCESS
		$config['file_name'] = $this->encrypt->sha1(md5($_FILES['file']['name'].now()));
		$this->load->library('upload',$config);
		$this->upload->initialize($config);

		$ref = $this->input->server('HTTP_REFERER', TRUE);
		if(!file_exists($config['upload_path']))
		{
			mkdir($config['upload_path'],0777);
		}
		if(file_exists($config['upload_path'].$config['file_name']))
		{
			$this->session->set_flashdata('result','ERROR: File already exists');
			redirect($ref,'location');
		}
		else
		{
			// SPECIAL VALIDATION FOR PDF FILES, ONLY PDF ARE ALLOWED TO BE UPLOADED IN THIS PART
			$pdf = array('application/pdf',
				'application/x-pdf',
				'application/x-download',
				'binary/octet-stream',
				'application/unknown',
				'application/force-download');
			foreach($pdf as $key => $value)
			{
				if($_FILES['file']['type'] == $value)
				{
					$file_allowed = TRUE;
				}
			}

			if($file_allowed == TRUE)
			{
				if($this->upload->do_upload('file'))
				{
					echo $this->upload->display_errors();
					$file = $this->upload->data();
					$user = $this->session->userdata('logged_in');

					switch($table)
					{
						case 'benefit_set_condition':
							$data = array(
									'condition_name' => str_replace('.pdf', '', $file['client_name']),
									'condition_details' => '',
									'filename' => $file['client_name'],
									'user' => $user['name']
									);
							$result = $this->records_model->register('benefits.benefit_set_condition',$data);
							break;

						case 'benefit_set_exclusion':
							$data = array(
									'exclusion_name' => str_replace('.pdf', '', $file['client_name']),
									'exclusion_details' => '',
									'filename' => $file['client_name'],
									'user' => $user['name']
									);
							$result = $this->records_model->register('benefits.benefit_set_exclusion',$data);
							break;

						default:
							break;
					}

					if($result)
					{
						$filedata = array(
								'uploaded_to' => $this->uri->segment(4),
								'uploader' => $user['name'],
								'date_uploaded' => mdate('%Y-%m-%d',now()),
								'hash' => $file['raw_name'],
								'filename' => $file['client_name'],
								'path' => $file['file_path'],
								'size' => $file['file_size'].' kB'
								);
						$resultFileData = $this->fileuploader_model->insert($filedata);

						if($resultFileData)
						{
							$this->session->set_flashdata('result','Succesfully uploaded file.');
							redirect($ref,'location');
						}
						else
						{
							$this->session->set_flashdata('result','ERROR: Inserting filedata error.');
							redirect($ref,'location');
						}
					}
					else
					{
						$this->session->set_flashdata('result','ERROR: Uploading file '.$this->upload->display_errors().'Please contact IT Department');
						redirect($ref,'location');
					}
				}
				else
				{
					$this->session->set_flashdata('result','ERROR: Uploading file '.$this->upload->display_errors().'Please contach IT Department');
					redirect($ref,'location');
				}
			}
			else
			{
				$this->session->set_flashdata('result',"ERROR: Only PDF file is allowed! You are uploading '".$_FILES['file']['type']."' filetype.");
				redirect($ref,'location');
			}
		}
	}

	function upload_attachment($attach_from,$table,$id)
	{
		// var_dump($_POST);
		// var_dump($_FILES);
		$result = $this->records_model->getRecordById($table,$id);

		switch ($attach_from)
		{
			case 'accounts_billing_company_patient_list':
				$config['upload_path'] = '/home/dev/web/operations051513/files/uploads/attachments/accounts/billing/company/patient_list';
				$data = array('attached_patient_list'=>true);
				$location = 'records/accounts/view/accounts_billing/'.$id;
				break;

			case 'accounts_billing_company_billing_statement':
				$config['upload_path'] = '/home/dev/web/operations051513/files/uploads/attachments/accounts/billing/company/billing_statement';
				$data = array('attached_billing_statement'=>true);
				$location = 'records/accounts/view/accounts_billing/'.$id;
				break;
			
			default:
				# code...
				break;
		}

		$date_created = date('Y-m-d');
		$user = $this->session->userdata('logged_in');

		$config['allowed_types'] = '*';
		$config['file_name'] = $this->encrypt->sha1(md5($_FILES['file']['name'].now()));
		$this->load->library('upload', $config);
		$this->upload->initialize($config);


		if(!file_exists($config['upload_path']))
		{
			mkdir($config['upload_path'], 0777);
		}
		if(file_exists($config['upload_path'].$config['file_name']))
		{
			$this->session->set_flashdata('result','<b>ERROR: File already exists.');
			redirect($location,'location');
		}
		else
		{
			$filetypes = array('application/pdf','application/x-pdf','application/x-download',
						'binary/octet-stream','application/unknown','application/force-download',
						'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/zip',
						'application/msword');

			foreach($filetypes as $key => $value)
			{
				if($_FILES['file']['type'] == $value)
				{
					$file_allowed = TRUE;
				}
			}

			if($file_allowed == TRUE)
			{
				if($this->upload->do_upload('file'))
				{
					echo $this->upload->display_errors();
					$file = $this->upload->data();

					if($file)
					{
						$update = $this->records_model->updateMultiField($table,'id',$id, $data);

						if($update)
						{
							switch ($attach_from)
							{
								case 'accounts_billing_company_patient_list':
								case 'accounts_billing_company_billing_statement':
									$filedata = array(
										'attach_from' => $attach_from,
										'attached_id' => $id,
										'reference_number' => $result[0]['reference_number'],
										'attached_by' => $user['name'],
										'hash' => $file['raw_name'],
										'filename' => $file['client_name'],
										'path' => $file['file_path'],
										'size' => $file['file_size'].' kB',
										'date_uploaded' => $date_created,
										'status' => 'ATTACHED'
										);
									break;
								
								default:
									# code...
									break;
							}
							
							$resultFileData = $this->records_model->register('attachments',$filedata);
						}
						

						if($resultFileData)
						{
							$this->session->set_flashdata('result', '<b>Succesfully attached file.</b>');
							redirect($location, 'refresh');
						}
						else
						{
							$this->session->set_flashdata('result', '<b>Error attaching file.</b>');
							redirect($location,'refresh');
						}
					}
					else
					{
						$this->session->set_flashdata('result','ERROR: Uploading file '.$this->upload->display_errors().'Please contact IT Department');
						redirect($location,'location');
					}
				}
				else
				{
					$this->session->set_flashdata('result','ERROR: Uploading file '.$this->upload->display_errors().'Please contach IT Department');
					redirect($location,'location');
				}
			}
			else
			{
				$this->session->set_flashdata('result',"ERROR: Please select a file!");
				redirect($location,'location');
			}
		}	
	}
}
?>