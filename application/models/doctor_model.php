<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include('application_model.php');

class Doctor_model extends Application_model {
  function __construct()
  {
    parent::__construct();
  }

  public function clinics($id)
  {
    $this->db->select("*");
    $this->db->from("establishments");
    $this->db->join('doctors_clinics', 'establishments.id = doctors_clinics.establishment_id', 'left');
    $this->db->where('doctors_clinics.doctor_id', $id);

    $query = $this->db->get();
    if($query->num_rows())
    {
      return $query->result_array();
    }
    else
    {
      return false;
    }
  }

  public function createClinic($doctor_id, $params)
  {
    if (!empty($params))
    {
      $this->db->insert('establishments', $params['establishment']);

      $data = array(
        "doctor_id"        => $doctor_id,
        "establishment_id" => $this->db->insert_id()
      );
      $this->db->insert('doctors_clinics', $data);

      return true;
    }
  }
}