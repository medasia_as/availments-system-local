<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Application_model extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }

  function all($table)
  {
    $this->db->select("*");
    $this->db->from($table);
    $query = $this->db->get();
    return $query->result_array();
  }

  function find($table, $id)
  {
    $this->db->select("*");
    $this->db->from($table);
    $this->db->where('id ', $id);
    $this->db->limit(1);

    $query  = $this->db->get();
    return $query->row_array();
  }

  function where($table, $params = [])
  {
    $this->db->select("*");
    $this->db->from($table);
    foreach ($params as $key => $value)
    {
      $this->db->where($key, $value);
    }
    $query = $this->db->get();
    return $query->result_array();
  }

  function last($table)
  {
    $this->db->select("*");
    $this->db->from($table);
    $this->db->order_by("id", "desc");
    $this->db->limit(1);

    $query  = $this->db->get();
    return $query->row_array();
  }
}